package com.vrubben.creditscore.processor;

import com.vrubben.creditscore.util.CreditScoreInfor;
import com.vrubben.creditscore.util.LoanInfor;
import java.security.SecureRandom;
import java.util.Random;
import java.util.concurrent.ConcurrentLinkedQueue;

public class LoansInforProcessorImpl implements LoansInforProcessor {

    private final ConcurrentLinkedQueue<LoanInfor> inputQueue;
    private final ConcurrentLinkedQueue<CreditScoreInfor> outputQueue;

    public LoansInforProcessorImpl(
            ConcurrentLinkedQueue<LoanInfor> inputQueue,
            ConcurrentLinkedQueue<CreditScoreInfor> outputQueue) {

        this.inputQueue = inputQueue;
        this.outputQueue = outputQueue;
    }

    /**
     * Marks the start of LoanInfors processing. This execution takes place in
     * the Main thread. This method reads LoanInfors from the input queue one by
     * one, processes them, then creates a CreditScoreInfor. The
     * CreditScoreInfor created is send to the output queue.
     *
     */
    @Override
    public void start() {

        Random randomValGenerator = new SecureRandom();

        this.inputQueue.forEach((nextLoanInfor) -> {
            // This sample code generates Credit Scores, randomly.
            // This is where future credit score calculations
            // should be handled.
            int score = randomValGenerator.nextInt(11);
            enQueueCreditScoreInfor(new CreditScoreInfor(
                    nextLoanInfor.getAccountHolderKey(),
                    score));
        });
    }

    /**
     * Adds a CreditScoreInfor into the output queue.
     *
     * @param creditScoreInfor an object containing infor about the credit score
     * of a single customer.
     */
    @Override
    public void enQueueCreditScoreInfor(CreditScoreInfor creditScoreInfor) {
        this.outputQueue.add(creditScoreInfor);
    }

}
