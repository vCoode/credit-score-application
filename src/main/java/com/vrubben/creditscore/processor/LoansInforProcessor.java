package com.vrubben.creditscore.processor;

import com.vrubben.creditscore.util.CreditScoreInfor;

public interface LoansInforProcessor {

    public void enQueueCreditScoreInfor(CreditScoreInfor creditScoreInfor);

    public void start();
}
