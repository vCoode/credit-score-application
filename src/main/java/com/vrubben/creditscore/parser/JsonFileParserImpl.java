package com.vrubben.creditscore.parser;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.vrubben.creditscore.util.LoanInfor;
import java.io.IOException;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JsonFileParserImpl implements JsonFileParser {

    private final Thread parseWorker;
    private final JsonParser jsonfileParser;
    private final ConcurrentLinkedQueue<LoanInfor> inputQueue;

    public JsonFileParserImpl(
            JsonParser jsonfileParser,
            ConcurrentLinkedQueue<LoanInfor> inputQueue) {

        this.jsonfileParser = jsonfileParser;
        this.inputQueue = inputQueue;
        this.parseWorker = new Thread(this);
    }

    /**
     * Adds a LoanInfor into the input queue.
     *
     * @param loanInfor an object containing information about a loan request.
     */
    @Override
    public void enQueueLoanInfor(LoanInfor loanInfor) {
        this.inputQueue.add(loanInfor);
    }

    /**
     * In a separate thread, this method parses a JSON file using Java's
     * Streaming API.
     */
    @Override
    public void run() {

        try {

            JsonToken startingToken = jsonfileParser.nextToken();
            JsonToken closingToken = startingToken == JsonToken.START_OBJECT
                    ? JsonToken.END_OBJECT : JsonToken.END_ARRAY;

            while (startingToken != closingToken) {

                jsonfileParser.nextToken();
                String currentKey = jsonfileParser.getCurrentName();
                if ("loanAccount".equals(currentKey)
                        && jsonfileParser.nextToken() == JsonToken.START_OBJECT) {

                    LoanInfor loanInfor = new LoanInfor();

                    while (jsonfileParser.nextToken() != JsonToken.END_OBJECT) {
                        currentKey = jsonfileParser.getText();
                        jsonfileParser.nextToken();
                        String currentVal = jsonfileParser.getText();

                        switch (currentKey) {
                            case "accountHolderKey":
                                loanInfor.setAccountHolderKey(currentVal);
                                break;
                            case "loanAmount":
                                loanInfor.setLoanAmount(Double.valueOf(currentVal));
                                break;
                            case "principalDue":
                                loanInfor.setPrincipalDue(Double.valueOf(currentVal));
                                break;
                            case "principalPaid":
                                loanInfor.setPrincipalPaid(Double.valueOf(currentVal));
                                break;
                            case "principalBalance":
                                loanInfor.setPrincipalBalance(Double.valueOf(currentVal));
                                break;
                            case "interestDue":
                                loanInfor.setInterestDue(Double.valueOf(currentVal));
                                break;
                            case "interestPaid":
                                loanInfor.setInterestPaid(Double.valueOf(currentVal));
                                break;
                            case "interestBalance":
                                loanInfor.setInterestBalance(Double.valueOf(currentVal));
                                break;
                            case "feesDue":
                                loanInfor.setFeesDue(Double.valueOf(currentVal));
                                break;
                            case "feesPaid":
                                loanInfor.setFeesPaid(Double.valueOf(currentVal));
                                break;
                            case "feesBalance":
                                loanInfor.setFeesBalance(Double.valueOf(currentVal));
                                break;
                            case "penaltyDue":
                                loanInfor.setPenaltyDue(Double.valueOf(currentVal));
                                break;
                            case "penaltyPaid":
                                loanInfor.setPenaltyPaid(Double.valueOf(currentVal));
                                break;
                            case "penaltyBalance":
                                loanInfor.setPenaltyBalance(Double.valueOf(currentVal));
                                break;
                        }
                    }
                    enQueueLoanInfor(loanInfor);
                }
                startingToken = jsonfileParser.nextToken();
            }

        } catch (IOException ex) {
            Logger.getLogger(JsonFileParserImpl.class.getName())
                    .log(Level.SEVERE, null, ex);
        }

        try {
            this.jsonfileParser.close();
        } catch (IOException ex) {
            Logger.getLogger(JsonFileParserImpl.class.getName())
                    .log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Starts the thread responsible for parsing input JSON files.
     */
    @Override
    public void start() {
        this.parseWorker.start();
    }

}
