package com.vrubben.creditscore.parser;

import com.vrubben.creditscore.util.LoanInfor;

public interface JsonFileParser extends Runnable {

    public void enQueueLoanInfor(LoanInfor loanInfor);
    public void start();

}
