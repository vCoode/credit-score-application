package com.vrubben.creditscore.writer;

import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.vrubben.creditscore.util.CreditScoreInfor;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CreditScoreInforWriterImpl implements CreditScoreInforWriter {

    private final Thread writerWorker;
    private final ConcurrentLinkedQueue<CreditScoreInfor> outputQueue;

    public CreditScoreInforWriterImpl(
            ConcurrentLinkedQueue<CreditScoreInfor> outputQueue) {

        this.writerWorker = new Thread(this);
        this.outputQueue = outputQueue;
    }

    /**
     * Starts the thread responsible for writing results to a file.
     */
    @Override
    public void start() {
        this.writerWorker.start();
    }

    /**
     * In a separate thread, this method reads data from the output queue and
     * writes the data to an output JSON file.
     */
    @Override
    public void run() {

        try (JsonGenerator scoresGenerator = new ObjectMapper()
                .enable(SerializationFeature.INDENT_OUTPUT)
                .getFactory()
                .createGenerator(new File("./Credit_Scores.json"), JsonEncoding.UTF8)) {

            // Write a list of scores
            scoresGenerator.writeStartArray();

            this.outputQueue.forEach((scoreInfor) -> {
                try {
                    scoresGenerator.writeObject(scoreInfor);
                } catch (IOException ex) {
                    Logger.getLogger(CreditScoreInforWriterImpl.class.getName())
                            .log(Level.SEVERE, null, ex);
                }
            });

            scoresGenerator.writeEndArray();
        } catch (IOException ex) {
            Logger.getLogger(CreditScoreInforWriterImpl.class.getName())
                    .log(Level.SEVERE, null, ex);
        }

    }

}
