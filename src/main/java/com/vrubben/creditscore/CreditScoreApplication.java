package com.vrubben.creditscore;

import com.vrubben.creditscore.parser.JsonFileParser;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.vrubben.creditscore.parser.JsonFileParserImpl;
import com.vrubben.creditscore.processor.LoansInforProcessor;
import com.vrubben.creditscore.processor.LoansInforProcessorImpl;
import com.vrubben.creditscore.util.CreditScoreInfor;
import com.vrubben.creditscore.util.LoanInfor;
import com.vrubben.creditscore.writer.CreditScoreInforWriter;
import com.vrubben.creditscore.writer.CreditScoreInforWriterImpl;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.ConcurrentLinkedQueue;

public class CreditScoreApplication {

    public static void main(String[] args) throws IOException {

        if (args.length != 1) {
            System.err.println("Input file not found.");
            return;
        }

        if (!Files.isReadable(Paths.get(args[0]))) {
            System.err.println("Input file provided cannot be read. Try again.");
            return;
        }

        JsonParser jsonParser = new JsonFactory().createParser(new File(args[0]));

        ConcurrentLinkedQueue<LoanInfor> inputQueue
                = new ConcurrentLinkedQueue<>();
        ConcurrentLinkedQueue<CreditScoreInfor> outputQueue
                = new ConcurrentLinkedQueue<>();

        JsonFileParser parser = new JsonFileParserImpl(
                jsonParser,
                inputQueue);

        LoansInforProcessor processor = new LoansInforProcessorImpl(
                inputQueue,
                outputQueue);

        CreditScoreInforWriter writer = new CreditScoreInforWriterImpl(
                outputQueue);

        parser.start();
        writer.start();
        processor.start();

    }

}
