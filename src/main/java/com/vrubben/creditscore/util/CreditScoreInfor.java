package com.vrubben.creditscore.util;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;

public class CreditScoreInfor implements Serializable {

    @JsonProperty("accountHolderKey")
    private String accountHolderKey;

    @JsonProperty("score")
    private Integer score;

    public CreditScoreInfor(String accountHolderKey, Integer score) {
        this.accountHolderKey = accountHolderKey;
        this.score = score;
    }

    public String getAccountHolderKey() {
        return accountHolderKey;
    }

    public void setAccountHolderKey(String accountHolderKey) {
        this.accountHolderKey = accountHolderKey;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

}
