package com.vrubben.creditscore.util;

import java.io.Serializable;

public class LoanInfor implements Serializable {

    private String accountHolderKey;
    private Double loanAmount;
    private Double principalDue;
    private Double principalPaid;
    private Double principalBalance;
    private Double interestDue;
    private Double interestPaid;
    private Double interestBalance;
    private Double feesDue;
    private Double feesPaid;
    private Double feesBalance;
    private Double penaltyDue;
    private Double penaltyPaid;
    private Double penaltyBalance;

    public String getAccountHolderKey() {
        return accountHolderKey;
    }

    public void setAccountHolderKey(String accountHolderKey) {
        this.accountHolderKey = accountHolderKey;
    }

    public Double getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(Double loanAmount) {
        this.loanAmount = loanAmount;
    }

    public Double getPrincipalDue() {
        return principalDue;
    }

    public void setPrincipalDue(Double principalDue) {
        this.principalDue = principalDue;
    }

    public Double getPrincipalPaid() {
        return principalPaid;
    }

    public void setPrincipalPaid(Double principalPaid) {
        this.principalPaid = principalPaid;
    }

    public Double getPrincipalBalance() {
        return principalBalance;
    }

    public void setPrincipalBalance(Double principalBalance) {
        this.principalBalance = principalBalance;
    }

    public Double getInterestDue() {
        return interestDue;
    }

    public void setInterestDue(Double interestDue) {
        this.interestDue = interestDue;
    }

    public Double getInterestPaid() {
        return interestPaid;
    }

    public void setInterestPaid(Double interestPaid) {
        this.interestPaid = interestPaid;
    }

    public Double getInterestBalance() {
        return interestBalance;
    }

    public void setInterestBalance(Double interestBalance) {
        this.interestBalance = interestBalance;
    }

    public Double getFeesDue() {
        return feesDue;
    }

    public void setFeesDue(Double feesDue) {
        this.feesDue = feesDue;
    }

    public Double getFeesPaid() {
        return feesPaid;
    }

    public void setFeesPaid(Double feesPaid) {
        this.feesPaid = feesPaid;
    }

    public Double getFeesBalance() {
        return feesBalance;
    }

    public void setFeesBalance(Double feesBalance) {
        this.feesBalance = feesBalance;
    }

    public Double getPenaltyDue() {
        return penaltyDue;
    }

    public void setPenaltyDue(Double penaltyDue) {
        this.penaltyDue = penaltyDue;
    }

    public Double getPenaltyPaid() {
        return penaltyPaid;
    }

    public void setPenaltyPaid(Double penaltyPaid) {
        this.penaltyPaid = penaltyPaid;
    }

    public Double getPenaltyBalance() {
        return penaltyBalance;
    }

    public void setPenaltyBalance(Double penaltyBalance) {
        this.penaltyBalance = penaltyBalance;
    }

}
