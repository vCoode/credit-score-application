# Threaded Credit Scoring App.

### Requirements
* JDK 1.7+
* Maven 3.0+
* Git

### Build and Run

#### Step 1

`git clone git@bitbucket.org:vCoode/credit-score-application.git && cd ./credit-score-application`

#### Step 2

`mvn clean compile assembly:single`

#### Step 3

`java -jar target/CreditScore-1.0-SNAPSHOT-jar-with-dependencies.jar /path/to/file.json`